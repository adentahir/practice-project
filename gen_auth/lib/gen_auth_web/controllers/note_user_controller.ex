defmodule GenAuthWeb.NoteUserController do
  use GenAuthWeb, :controller

  alias GenAuth.Catalogue
  alias GenAuth.Accounts

  def index(conn, _params) do
    user = conn.assigns.current_user

    %{notes: notes}  = Catalogue.list_user_notes(user)
     IO.inspect(notes, label: "notes")

    render(conn, "index.html", notes: notes)
  end

  def permission(conn, _params) do
    note_users = Catalogue.get_note_user!(conn.assigns.users)
    changeset = Catalogue.change_note_user(note_users)
    render(conn, "permission.html", changeset)
  end



  def update(conn, %{"id" => id, "note" => note_params}) do
    note = Catalogue.get_note!(id)

    case Catalogue.update_note(note, note_params) do
      {:ok, note} ->
        conn
        |> put_flash(:info, "note updated successfully.")
        |> redirect(to: Routes.note_path(conn, :index))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", note: note, changeset: changeset)
    end
  end

  def edit(conn, %{"id" => note_id}) do

    users = Accounts.get_users()
    current_user_email = conn.assigns.current_user.email
    current_user_id = conn.assigns.current_user.id
    note = Catalogue.get_note!(note_id)

      # note_user_changeset =
      # NoteUsers
      # |> Ecto.Changeset.put_assoc(:user, conn.assigns.current_user)
    Catalogue.edit_note_user(current_user_id, note_id)

    list =
  #  users
  #  |> Enum.map(fn x ->  Map.take(x, [:email, :id]) end)
  #  |> Enum.map(fn x ->  Enum.reduce(x, fn({key, value}), _acc -> {key, value} end) end)
  #  |> IO.inspect()

  users
   |> Enum.map(fn %{email: email, id: id} -> {String.to_atom(email), id} end)
  #  |> Keyword.delete(current_email)
   |> IO.inspect()
  #  |> IO.inspect()
  #  |> Keyword.keyword?()
  #  |> IO.inspect()

#    list |>
# Enum.group_by(fn el -> {el.email, el.id} end) |>
# Enum.map(fn {{a, b}, v} ->
# Enum.reduce(v, [email: a, id: b])
# end)
   # IO.inspect(Map.keys(users))
   #  IO.inspect(Keyword.keyword?(users))
    # [id: id, email: email] = users
    # # [id: id, email: email] = users

    # #  id = Keyword.get(users, :email)
    #  IO.inspect(id)
    #  IO.inspect(email)

    # note_user_changeset =
    #   note_user
    #   |> Ecto.build_assoc(:comment)
    #   |> Ecto.Changeset.change()
    #   |> Ecto.Changeset.put_assoc(:user, user)

    render(conn, "edit.html", users: list)
  end

  def create(conn, %{"note_user_note" => note_user_params}) do
    current_user = conn.assigns.current_user
    changeset = Ecto.build_assoc(current_user, :notes, note_user_params)

    case Catalogue.create_user_note(note_user_params) do

      {:ok, note} ->
     conn
      |> put_flash(:info, "A New Note has been created")
      |> redirect(to: Routes.note_path(conn, :index))
      {:error, _changeset} ->
        conn
        |> put_flash(:error, "Note Creation failed")
        |> redirect(to: Routes.note_path(conn, :index))
    end

    render(conn, "new.html" )

   _user_id = conn.assigns.current_user.id


  end

end
