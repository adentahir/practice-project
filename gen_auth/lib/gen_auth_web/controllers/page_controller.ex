defmodule GenAuthWeb.PageController do
  use GenAuthWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end


end
