defmodule GenAuth.Catalogue do
  @moduledoc """
  The Catalogue context.
  """

  import Ecto.Query, warn: false
  alias GenAuth.Repo

  alias GenAuth.Catalogue.{Note, NoteUsers}

  @doc """
  Returns the list of notes.

  ## Examples

      iex> list_notes()
      [%Note{}, ...]

  """
  def list_notes do
    Repo.all(Note)
  end

  def list_user_notes(user) do
    Repo.preload(user, :notes)
  end

  def list_users_email(note_user) do
    Repo.preload note_user, [:note, :user]
  end

  @doc """
  Gets a single note.

  Raises `Ecto.NoResultsError` if the Note does not exist.

  ## Examples

      iex> get_note!(123)
      %Note{}

      iex> get_note!(456)
      ** (Ecto.NoResultsError)

  """
  def get_note!(id), do: Repo.get!(Note, id)

  @doc """
  Creates a note.

  ## Examples

      iex> create_note(%{field: value})
      {:ok, %Note{}}

      iex> create_note(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_note(note, attrs \\ %{}) do
    note
    |> Note.changeset(attrs)
    |> Repo.insert()
  end





  @doc """
  Updates a note.

  ## Examples

      iex> update_note(note, %{field: new_value})
      {:ok, %Note{}}

      iex> update_note(note, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_note(%Note{} = note, attrs) do
    note
    |> Note.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a note.

  ## Examples

      iex> delete_note(note)
      {:ok, %Note{}}

      iex> delete_note(note)
      {:error, %Ecto.Changeset{}}

  """
  def delete_note(note) do
    Repo.delete(note)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking note changes.

  ## Examples

      iex> change_note(note)
      %Ecto.Changeset{data: %Note{}}

  """
  def change_note(%Note{} = note, attrs \\ %{}) do
    Note.changeset(note, attrs)
  end

  @doc """
  Returns the list of note_user.

  ## Examples

      iex> list_note_user()
      [%NoteUsers{}, ...]

  """
  def list_note_user do
    Repo.all(NoteUsers)
  end

  @doc """
  Gets a single note_user.

  Raises `Ecto.NoResultsError` if the Allow email does not exist.

  ## Examples

      iex> get_note_user!(123)
      %NoteUsers{}

      iex> get_note_user!(456)
      ** (Ecto.NoResultsError)

  """
  def get_note_user!(id), do: Repo.get!(NoteUsers, id)

  @doc """
  Creates a note_user.

  ## Examples

      iex> create_note_user(%{field: value})
      {:ok, %NoteUsers{}}

      iex> create_note_user(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_note_user(attrs \\ %{}) do
    %NoteUsers{}
    |> NoteUsers.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a note_user.

  ## Examples

      iex> update_note_user(note_user, %{field: new_value})
      {:ok, %NoteUsers{}}

      iex> update_note_user(note_user, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_note_user(%NoteUsers{} = note_user, attrs) do
    note_user
    |> NoteUsers.changeset(attrs)
    |> Repo.update()
  end

  @spec edit_note_user(any, any) :: {any, nil | list}
  def edit_note_user( user_id, note_id) do
    changeset = NoteUsers.changeset(%NoteUsers{}, %{user_id: user_id, note_id: note_id})
    
    Ecto.Multi.new()
    |> Ecto.Multi.insert_or_update(:insert_or_update, changeset)
    |> GenAuth.Repo.transaction()

  end

  @doc """
  Deletes a note_user.

  ## Examples

      iex> delete_note_user(note_user)
      {:ok, %NoteUsers{}}

      iex> delete_note_user(note_user)
      {:error, %Ecto.Changeset{}}

  """
  def delete_note_user(%NoteUsers{} = note_user) do
    Repo.delete(note_user)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking note_user changes.

  ## Examples

      iex> change_note_user(note_user)
      %Ecto.Changeset{data: %NoteUsers{}}

  """
  def change_note_user(%NoteUsers{} = note_user, attrs \\ %{}) do
    NoteUsers.changeset(note_user, attrs)
  end
end
