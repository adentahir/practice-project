defmodule GenAuth.Repo.Migrations.CreateNotes do
  use Ecto.Migration

  def up do
    create table(:notes) do

      add :user_id, references(:users)
      add :note_title, :string
      add :note_discription, :text

      timestamps()
    end
  end

  def down do
    drop table(:notes)
  end
  end
