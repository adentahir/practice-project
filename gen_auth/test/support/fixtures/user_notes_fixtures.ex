defmodule GenAuth.UserNotesFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `GenAuth.UserNotes` context.
  """

  @doc """
  Generate a unique note user_uuid.
  """
  def unique_note_user_uuid do
    raise "implement the logic to generate a unique note user_uuid"
  end

  @doc """
  Generate a note.
  """
  def note_fixture(attrs \\ %{}) do
    {:ok, note} =
      attrs
      |> Enum.into(%{
        user_uuid: unique_note_user_uuid()
      })
      |> GenAuth.UserNotes.create_note()

    note
  end

  @doc """
  Generate a allow_email.
  """
  def allow_email_fixture(attrs \\ %{}) do
    {:ok, allow_email} =
      attrs
      |> Enum.into(%{
        user_email: "some user_email"
      })
      |> GenAuth.UserNotes.create_allow_email()

    allow_email
  end
end
