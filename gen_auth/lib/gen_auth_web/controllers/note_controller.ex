defmodule GenAuthWeb.NoteController do
  use GenAuthWeb, :controller

  alias GenAuth.Catalogue
  alias GenAuth.Catalogue.Note

  def index(conn, _params) do
    user = conn.assigns.current_user

    %{notes: notes}  = Catalogue.list_user_notes(user)
     IO.inspect(notes, label: "notes")

    render(conn, "index.html", notes: notes)
  end

  def create(conn, %{"note" => note_params}) do
    current_user = conn.assigns.current_user
    changeset = Ecto.build_assoc(current_user, :notes, note_params)

    case Catalogue.create_note(changeset, note_params) do

      {:ok, note} ->
     conn
      |> put_flash(:info, "A New Note has been created")
      |> redirect(to: Routes.note_path(conn, :index))
      {:error, _changeset} ->
        conn
        |> put_flash(:error, "Note Creation failed")
        |> redirect(to: Routes.note_path(conn, :index))
    end

    render(conn, "new.html" )

   _user_id = conn.assigns.current_user.id


  end

  def update(conn, %{"id" => id, "note" => note_params}) do
    note = Catalogue.get_note!(id)

    case Catalogue.update_note(note, note_params) do
      {:ok, note} ->
        conn
        |> put_flash(:info, "note updated successfully.")
        |> redirect(to: Routes.note_path(conn, :index))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", note: note, changeset: changeset)
    end
  end

  def edit(conn, %{"id" => id}) do
    note = Catalogue.get_note!(id)
    changeset = Catalogue.change_note(note)
    IO.inspect(changeset)
    render(conn, "edit.html", note: note, changeset: changeset)
  end

  def new(conn, _params) do
    changeset = Catalogue.change_note(%Note{})

    render(conn, "new.html", changeset: changeset)
  end


  def delete(conn, %{"id" => id}) do
     note = Catalogue.get_note!(id)
     Catalogue.delete_note(note)
        conn
      |> put_flash(:info, "Note has been deleted")
      |> redirect(to: Routes.note_path(conn, :index))
  end





end
