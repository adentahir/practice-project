defmodule GenAuth.Catalogue.Note do
  use Ecto.Schema
  import Ecto.Changeset

  schema "notes" do
    field :note_title, :string
    field :note_discription, :string

    belongs_to :user, GenAuth.Accounts.User
    many_to_many :note_users, GenAuth.Accounts.User, join_through: "note_users"

    # has_many :allow_email, GenAuth.UserNotes.AllowEmail

    timestamps()
  end

  @doc false
  def changeset(note, attrs) do
    note
    |> cast(attrs, [:note_title, :note_discription])
    |> validate_required([:note_title])
  end
end
