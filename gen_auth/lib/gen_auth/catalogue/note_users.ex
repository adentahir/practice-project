defmodule GenAuth.Catalogue.NoteUsers do
  use Ecto.Schema
  import Ecto.Changeset

  schema "note_users" do
    belongs_to :user, GenAuth.Accounts.User
    belongs_to :note, GenAuth.Catalogue.Note


    timestamps()
  end

  @doc false
  def changeset(note_user, attrs) do
    note_user
    |> cast(attrs, [:note_id, :user_id ])
    |> validate_required([:note_id, :user_id])
  end


end
