defmodule GenAuth.UserNotesTest do
  use GenAuth.DataCase

  alias GenAuth.UserNotes

  describe "notes" do
    alias GenAuth.UserNotes.Note

    import GenAuth.UserNotesFixtures

    @invalid_attrs %{user_uuid: nil}

    test "list_notes/0 returns all notes" do
      note = note_fixture()
      assert UserNotes.list_notes() == [note]
    end

    test "get_note!/1 returns the note with given id" do
      note = note_fixture()
      assert UserNotes.get_note!(note.id) == note
    end

    test "create_note/1 with valid data creates a note" do
      valid_attrs = %{user_uuid: "7488a646-e31f-11e4-aace-600308960662"}

      assert {:ok, %Note{} = note} = UserNotes.create_note(valid_attrs)
      assert note.user_uuid == "7488a646-e31f-11e4-aace-600308960662"
    end

    test "create_note/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = UserNotes.create_note(@invalid_attrs)
    end

    test "update_note/2 with valid data updates the note" do
      note = note_fixture()
      update_attrs = %{user_uuid: "7488a646-e31f-11e4-aace-600308960668"}

      assert {:ok, %Note{} = note} = UserNotes.update_note(note, update_attrs)
      assert note.user_uuid == "7488a646-e31f-11e4-aace-600308960668"
    end

    test "update_note/2 with invalid data returns error changeset" do
      note = note_fixture()
      assert {:error, %Ecto.Changeset{}} = UserNotes.update_note(note, @invalid_attrs)
      assert note == UserNotes.get_note!(note.id)
    end

    test "delete_note/1 deletes the note" do
      note = note_fixture()
      assert {:ok, %Note{}} = UserNotes.delete_note(note)
      assert_raise Ecto.NoResultsError, fn -> UserNotes.get_note!(note.id) end
    end

    test "change_note/1 returns a note changeset" do
      note = note_fixture()
      assert %Ecto.Changeset{} = UserNotes.change_note(note)
    end
  end

  describe "allow_email" do
    alias GenAuth.UserNotes.AllowEmail

    import GenAuth.UserNotesFixtures

    @invalid_attrs %{user_email: nil}

    test "list_allow_email/0 returns all allow_email" do
      allow_email = allow_email_fixture()
      assert UserNotes.list_allow_email() == [allow_email]
    end

    test "get_allow_email!/1 returns the allow_email with given id" do
      allow_email = allow_email_fixture()
      assert UserNotes.get_allow_email!(allow_email.id) == allow_email
    end

    test "create_allow_email/1 with valid data creates a allow_email" do
      valid_attrs = %{user_email: "some user_email"}

      assert {:ok, %AllowEmail{} = allow_email} = UserNotes.create_allow_email(valid_attrs)
      assert allow_email.user_email == "some user_email"
    end

    test "create_allow_email/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = UserNotes.create_allow_email(@invalid_attrs)
    end

    test "update_allow_email/2 with valid data updates the allow_email" do
      allow_email = allow_email_fixture()
      update_attrs = %{user_email: "some updated user_email"}

      assert {:ok, %AllowEmail{} = allow_email} =
               UserNotes.update_allow_email(allow_email, update_attrs)

      assert allow_email.user_email == "some updated user_email"
    end

    test "update_allow_email/2 with invalid data returns error changeset" do
      allow_email = allow_email_fixture()

      assert {:error, %Ecto.Changeset{}} =
               UserNotes.update_allow_email(allow_email, @invalid_attrs)

      assert allow_email == UserNotes.get_allow_email!(allow_email.id)
    end

    test "delete_allow_email/1 deletes the allow_email" do
      allow_email = allow_email_fixture()
      assert {:ok, %AllowEmail{}} = UserNotes.delete_allow_email(allow_email)
      assert_raise Ecto.NoResultsError, fn -> UserNotes.get_allow_email!(allow_email.id) end
    end

    test "change_allow_email/1 returns a allow_email changeset" do
      allow_email = allow_email_fixture()
      assert %Ecto.Changeset{} = UserNotes.change_allow_email(allow_email)
    end
  end
end
