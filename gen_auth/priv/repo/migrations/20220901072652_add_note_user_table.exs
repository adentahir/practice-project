defmodule GenAuth.Repo.Migrations.AddNoteUserTable do
  use Ecto.Migration

  def down do
    drop index(:note_users, [:note_id])
    drop index(:note_users, [:user_id])
    drop index(:note_users, [:user_id, :note_id])
    drop table(:note_users)
  end

  def up do
    create table(:note_users) do
      add :user_id, references(:users)
      add :note_id, references(:notes)
      timestamps()
    end

    create index(:note_users, [:note_id])
    create index(:note_users, [:user_id])
    create index(:note_users, [:user_id, :note_id])
  end



end
